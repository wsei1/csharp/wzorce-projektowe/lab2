﻿namespace Lab2
{
	interface IFunction
    {
        int Id { get; }
        string Name { get; }
        decimal GetY(decimal x);
    }
}

﻿namespace Lab2
{
	class Rectangle : ICalculator
    {
        decimal result;
        decimal y;
        public string Name => "Całka metody kwadratów";

        public decimal GetIntegralValue(IFunction function, decimal rangeFrom, decimal rangeTo)
        {
            for (decimal i = rangeFrom; i < rangeTo;)
            {
                y = function.GetY(i);
                result += (y * 0.0001m);
                i += 0.0001m;
            }
            return result;
        }
    }
}

﻿namespace Lab2
{
	class Trapeze : ICalculator
    {
        public string Name => "Całka metody trapezów";

        public decimal GetIntegralValue(IFunction function, decimal rangeFrom, decimal rangeTo)
        {
            const decimal h = 0.1m;
            decimal result = 0;

            for (var i = rangeFrom; i < rangeTo;)
            {
                var a = function.GetY(i);
                var b = function.GetY(i + h);

                var trapezeField = ((a + b) * h) / 2;
                result += trapezeField;
                i += h;
            }
            return result;
        }
    }
}

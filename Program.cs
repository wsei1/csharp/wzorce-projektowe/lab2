﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lab2
{
	class Program
	{
		static void Main(string[] args)
		{
			int minValue = 0;
			int maxValue = 0;
			List<IFunction> Functions = new List<IFunction>
			{
				new Function1(),
				new Function2(),
				new Function3(),
				new Function4()
			};

			List<ICalculator> Calculators = new List<ICalculator>
			{
				new Trapeze(),
				new Rectangle()
			};

			Console.WriteLine("Wybierz dla której funkcji, chcesz liczyć całkę.");
			Console.WriteLine("Dostępne funkcje:");

			foreach (IFunction funcData in Functions)
			{
				Console.WriteLine("ID: {0}, Nazwa: {1}", funcData.Id, funcData.Name);
			}

			var choose = int.Parse(Console.ReadLine());

			if (choose > Functions.Count + 1 || choose == 0)
			{
				Console.WriteLine("Wpisales niepoprawna liczbe!");
				Console.ReadKey();
				return;
			}

			Console.WriteLine("Dla ktorego przedzialu chcesz policzyc całkę?");
			Console.WriteLine("Przedzial 1: od -10 do 10");
			Console.WriteLine("Przedzial 2: od -5 do 20");
			Console.WriteLine("Przedzial 3: od -5 do 0");

			var rangeChoose = int.Parse(Console.ReadLine());

			switch (rangeChoose)
			{
				case 1:
					minValue = -10;
					maxValue = 10;
					break;
				case 2:
					minValue = -5;
					maxValue = 20;
					break;
				case 3:
					minValue = -5;
					maxValue = 0;
					break;
				default:
					Console.WriteLine("Wybrałeś złą liczbę!");
					Console.ReadKey();
					return;
			}

			Calculate(Calculators, Functions, choose, minValue, maxValue);
			
			Console.ReadKey();
		}

		private static void Calculate(List<ICalculator> Calculators, List<IFunction> functions, int choose, int minValue, int maxValue)
		{
			foreach (var calculator in Calculators)
			{
				Console.WriteLine(calculator.Name);
				Console.WriteLine(calculator.GetIntegralValue(functions.First(x => x.Id == choose), minValue, maxValue));
			}
		}
	}
}

﻿namespace Lab2
{
	public class Function4 : IFunction
	{
		public int Id => 4;

		public string Name => "y=3x^2 + 2x + 3";

		public decimal GetY(decimal x)
		{
			return 3 * (x * x) + 2 * x + 3;
		}
	}
}
